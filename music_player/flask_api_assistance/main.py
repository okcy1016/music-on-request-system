from flask import Flask, redirect, request, send_from_directory
import requests

app = Flask(__name__)

@app.route("/get_redirect_song_url_by_id")
def get_redirect_song_url_by_id():
    song_id = request.args.get("id")
    response_dict = requests.get("https://inncu.cn/song/url", params={"id": song_id}).json()
    if response_dict["code"] == 200:
        song_url_to_redirect = response_dict["data"][0]["url"]
    return redirect(song_url_to_redirect, code=302)

@app.route("/get_redirect_lrc_by_id")
def get_redirect_lrc_by_id():
    song_id = request.args.get("id")
    response_dict = requests.get("https://inncu.cn/lyric", params={"id": song_id}).json()
    if response_dict["code"] == 200:
        lrc_string = response_dict["lrc"]["lyric"]
    else:
        print("fetch lyric failed!")
    # write lrc_string to file
    with open("lyric.lrc", "w") as fpw:
        fpw.write(lrc_string)
    return send_from_directory(".", "lyric.lrc")


if __name__ == "__main__":
    app.run(port=1521)
