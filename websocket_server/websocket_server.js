const WebSocket = require('ws');

const wss = new WebSocket.Server({ host: "127.0.0.1", port: 1520 });

wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
	console.log('received message.');
	// Broadcast to everyone else.
	wss.clients.forEach(function each(client) {
	    if (client !== ws && client.readyState === WebSocket.OPEN) {
		client.send(message);
	    }
	});
    });
    ws.send("websocket ok.");
});
